#include <iostream>

//#include <CGAL/Simple_cartesian.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/triangulate_polyhedron.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef CGAL::Polygon_2<K> Polygon_2;
typedef Polygon_2::Vertex_iterator VertexIterator;
typedef Polygon_2::Edge_const_iterator EdgeIterator;

typedef K::Point_3 Point_3;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef Polyhedron Polygon_3;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_handle Halfedge_handle;
typedef Polyhedron::HalfedgeDS HalfedgeDS;

using namespace std;


#include "print_utils.h"



template<class HDS>
class Extrusion : public CGAL::Modifier_base<HDS> {
public:
//    std::vector<CGAL::HalfedgeDS_halfedge_base::Vertex> w_;
    std::vector<Point_3> w_;//TODO: investige receiving polygon rather than vector of points. (enables circulator concept)
    Polygon_2 p_2;
    Polygon_3 p_3; //need to insure it is a plane -> .isPlane()?
    Polyhedron ph_;

    double z_offset = 0;
    double z_thickness = 1;

    Extrusion() { }

    //TODO: constructor sets array into vector BUT actually want to pass in points that dont change
    Extrusion(Point_3 *w, int len) {
        w_.assign(w, w + len); //is 'len' necessary
        cout << "Extrusion construct from Point_3 array" << endl;
        cout << "array size : " << w_.size() << endl;
//        if w_
    }
    Extrusion(Polygon_2 *p) {
        w_.assign(p, p + p->size());//todo: does this actually work?
    }
//    Prism_builder(Polygon_3 p_3, int len) {
////        for_each(p_3->vertices_begin(),p_3->vertices_end())
//        for (int j = 0; j < p_3.size_of_vertices(); j++) {
//            p_2.insert(p_3->,p_3.vertices_end());
//        }
//    }
    void set_data(Point_3 *w, int len) {
        w_.assign(w, w + len);
    }

    void set_offset(double o_) {
        z_offset = o_;
    }
    void set_thickness(double t_) {
        z_thickness = t_;
    }
    Polygon_2 get_seed_polygon(){
        return p_2;
    }
//    Polygon_3 get_seed_polygon(){
//        return p_3;
//    }

    void operator()(HDS &hds) {
        // Postcondition: hds is a valid polyhedral surface.
        typedef typename HDS::Vertex Vertex;
        typedef typename Vertex::Point Point;

        /**
         * TODO : Validate input shape
         *   - that it is a polygon (begin = end) (note : needs to remove last point if so..)
         *   - that it is simple
         */

        //create cgal incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

        B.begin_surface(w_.size(), (2 + w_.size()), (2 * w_.size() + 4 * w_.size()));
        /**
         * extruded polyhedron has:
         *  halfedges : 2*polygon.size + 4*polygon.size()
         *  facets    : 2 + polygon.size() facets
         *  vertices  : 2*polygon.size()
         */
        {
            //add all the points
            for (int i = 0; i < w_.size(); i++) {
                B.add_vertex(Point(w_[i].x(), w_[i].y(), (w_[i].z() + z_offset)));
                B.add_vertex(Point(w_[i].x(), w_[i].y(), (w_[i].z() + z_thickness + z_offset)));
            }
            //define the n = w_.size()-1 amount of side faces
            for (int i = 0; i < (w_.size()-1)*2; i+=2) {//TODO : Division gives unpredictable behaviour?
                cout << "adding vertex " << i << endl;
                B.begin_facet();
                B.add_vertex_to_facet(i);
                B.add_vertex_to_facet(i+1);
                B.add_vertex_to_facet(i+3);//note order
                B.add_vertex_to_facet(i+2);
                B.end_facet();
            }
            //define last side face : todo: could be replaced if above loop uses Circulator as i
            cout << "adding facet nr " << (w_.size() * 2) << endl;
            B.begin_facet();
            B.add_vertex_to_facet((w_.size()*2)-2);
            B.add_vertex_to_facet((w_.size()*2)-1);
            B.add_vertex_to_facet(1);//note order
            B.add_vertex_to_facet(0);
            B.end_facet();
            cout << "added last side facet "  << endl;

            //define top and bottom faces //Todo: might need to triangulate this part already before function receives
//            for(int j = 0;j < 2; j++){
                //bottom => j = 0 ; top => j = 1
                B.begin_facet();
                cout << "facet " << 0 << " : ";
                for (int i = 0; i < w_.size(); i++) {
                    cout << " " << (i*2);
                    B.add_vertex_to_facet((i*2));
                }
                cout << endl;
                B.end_facet();
                B.begin_facet();
                cout << "facet " << 1 << " :";
                for (int i = w_.size()-1; i >= 0 ; i--) {
                    cout << " " << (i*2)+1;
                    B.add_vertex_to_facet((i*2)+1);
                }
                cout << endl;
                B.end_facet();

        }
        B.end_surface();
    }
};



int     main(int argc, char *argv[]) {


//    static int x = 1000;
//    static Point_3 xyz_list[18] = {
//            Point_3(2500, 1100, 0),
//            Point_3(2000, 1200, 0),
//            Point_3(1600, 1500, 0),
//            Point_3(1300, 1900, 0),
//            Point_3(1200, 2500, 0),
//            Point_3(1300, 3000, 0),
//            Point_3(1600, 3400, 0),
//            Point_3(1900, 3600, 0),
//            Point_3(2600, 3800, 0),
//            Point_3(3200, 3600, 0),
//            Point_3(3500, 3400, 0),
//            Point_3(3800, 3000, 0),
//            Point_3(3900, 2500, 0),
//            Point_3(3800, 2000, 0),
//            Point_3(3500, 1500, 0),
//            Point_3(3100, 1200, 0),
//            Point_3(2600, 1100, 0),
//            Point_3(2500, 1100, 0)//,
//
//    };
    static Point_3 xyz_cube[4] = {
            Point_3(0,0,0),
            Point_3(0,10,0),
            Point_3(10,10,0),
            Point_3(10,0,0)
    };
//    static Point_3 xyz_plane[4] = {
//            Point_3(0,0,0),
//            Point_3(0,1,0),
//            Point_3(1,0,0),
//            Point_3(0,0,0)
//    };
    Polyhedron P;
    Extrusion<HalfedgeDS> prism(xyz_cube,4);
    prism.z_thickness = 2;
    P.delegate( prism);


    Vertex_iterator begin = P.vertices_begin();
    for ( ; begin != P.vertices_end(); ++begin)
        std::cout << "(" << begin->point() << ") ";
    std::cout << std::endl;
    return 0;

}


//    Point_2 p(1,1), q(10,10);
//    std::cout << "p = " << p << std::endl;
//    std::cout << "q = " << q.x() << " " << q.y() << std::endl;
//    std::cout << "&q = " << &q.x() << " " << &q.y() << std::endl;
//
//    std::cout << "sqdist(p,q) = "
//    << CGAL::squared_distance(p,q) << std::endl;
//
//    Segment_2 s(p,q);
//    Point_2 m(5, 9);
//
//    std::cout << "m = " << m << std::endl;
//    std::cout << "sqdist(Segment_2(p,q), m) = "
//    << CGAL::squared_distance(s,m) << std::endl;
//    std::cout << "p, q, and m ";
//    switch (CGAL::orientation(p,q,m)){
//        case CGAL::COLLINEAR:
//            std::cout << "are collinear\n";
//            break;
//        case CGAL::LEFT_TURN:
//            std::cout << "make a left turn\n";
//            break;
//        case CGAL::RIGHT_TURN:
//            std::cout << "make a right turn\n";
//            break;
//    }
//    std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;
