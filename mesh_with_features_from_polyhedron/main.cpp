#include <iostream>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/triangulate_polyhedron.h>


typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_with_features_3.h>
#include <CGAL/make_mesh_3.h>

// IO
#include <CGAL/IO/Polyhedron_iostream.h>
// Domain

typedef CGAL::Mesh_polyhedron_3<K>::type Polyhedron;
typedef CGAL::Polyhedral_mesh_domain_with_features_3<K> Mesh_domain;

//for incremental building
typedef CGAL::Polygon_2<K> Polygon_2;
typedef K::Point_3 Point_3;
typedef Polyhedron Polygon_3;
typedef Polyhedron::HalfedgeDS HalfedgeDS;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_handle Halfedge_handle;


// Triangulation
#ifdef CGAL_CONCURRENT_MESH_3
  typedef CGAL::Mesh_triangulation_3<
    Mesh_domain,
    CGAL::Kernel_traits<Mesh_domain>::Kernel, // Same as sequential
    CGAL::Parallel_tag                        // Tag to activate parallelism
  >::type Tr;
#else
typedef CGAL::Mesh_triangulation_3<Mesh_domain>::type Tr;
#endif


typedef CGAL::Mesh_complex_3_in_triangulation_3<
        Tr,Mesh_domain::Corner_index,Mesh_domain::Surface_patch_index > C3t3;
// Criteria
typedef C3t3::Facets_in_complex_iterator C3t3_Facet_Iterator;
typedef C3t3::Vertices_in_complex_iterator C3t3_Vertex_Iterator;
//typedef C3t3::Vertices_in_complex_iterator C3t3_Vertex_Iterator;


typedef CGAL::Mesh_criteria_3<Tr> Mesh_criteria;


typedef CGAL::Mesh_edge_criteria_3< Tr > 	Edge_criteria;
typedef CGAL::Mesh_facet_criteria_3< Tr > Facet_criteria;
typedef CGAL::Mesh_cell_criteria_3< Tr > 	Cell_criteria;

// To avoid verbose function and named parameters call
using namespace CGAL::parameters;

//using namespace std;
#include "print_utils.h"



using std::cout;
using std::endl;

template<class HDS>
class Extrusion : public CGAL::Modifier_base<HDS> {
public:
//    std::vector<CGAL::HalfedgeDS_halfedge_base::Vertex> w_;
    std::vector<Point_3> w_;//TODO: investige receiving polygon rather than vector of points. (enables circulator concept)
    Polygon_2 p_2;
    Polygon_3 p_3; //need to insure it is a plane -> .isPlane()?
    Polyhedron ph_;

    double z_offset = 0;
    double z_thickness = 1;

    Extrusion() { }

    //TODO: constructor sets array into vector BUT actually want to pass in points that dont change
    Extrusion(Point_3 *w, int len) {
        w_.assign(w, w + len); //is 'len' necessary
        cout << "Extrusion construct from Point_3 array" << endl;
        cout << "array size : " << w_.size() << endl;
//        if w_
    }
    Extrusion(Polygon_2 *p) {
        w_.assign(p, p + p->size());//todo: does this actually work?
    }
//    Prism_builder(Polygon_3 p_3, int len) {
////        for_each(p_3->vertices_begin(),p_3->vertices_end())
//        for (int j = 0; j < p_3.size_of_vertices(); j++) {
//            p_2.insert(p_3->,p_3.vertices_end());
//        }
//    }
    void set_data(Point_3 *w, int len) {
        w_.assign(w, w + len);
    }

    void set_offset(double o_) {
        z_offset = o_;
    }
    void set_thickness(double t_) {
        z_thickness = t_;
    }
    Polygon_2 get_seed_polygon(){
        return p_2;
    }
//    Polygon_3 get_seed_polygon(){
//        return p_3;
//    }

    void operator()(HDS &hds) {
        // Postcondition: hds is a valid polyhedral surface.
        typedef typename HDS::Vertex Vertex;
        typedef typename Vertex::Point Point;

        /**
         * TODO : Validate input shape
         *   - that it is a polygon (begin = end) (note : needs to remove last point if so..)
         *   - that it is simple
         */

        //create cgal incremental builder
        CGAL::Polyhedron_incremental_builder_3<HDS> B(hds, true);

        B.begin_surface(w_.size(), (2 + w_.size()), (2 * w_.size() + 4 * w_.size()));
        /**
         * extruded polyhedron has:
         *  halfedges : 2*polygon.size + 4*polygon.size()
         *  facets    : 2 + polygon.size() facets
         *  vertices  : 2*polygon.size()
         */
        {
            //add all the points
            for (int i = 0; i < w_.size(); i++) {
                B.add_vertex(Point(w_[i].x(), w_[i].y(), (w_[i].z() + z_offset)));
                B.add_vertex(Point(w_[i].x(), w_[i].y(), (w_[i].z() + z_thickness + z_offset)));
            }
            //define the n = w_.size()-1 amount of side faces
            for (int i = 0; i < (w_.size()-1)*2; i+=2) {//TODO : Division gives unpredictable behaviour?
                cout << "adding vertex " << i << endl;
                B.begin_facet();
                B.add_vertex_to_facet(i);
                B.add_vertex_to_facet(i+1);
                B.add_vertex_to_facet(i+3);//note order
                B.add_vertex_to_facet(i+2);
                B.end_facet();
            }
            //define last side face : todo: could be replaced if above loop uses Circulator as i
            cout << "adding facet nr " << (w_.size() * 2) << endl;
            B.begin_facet();
            B.add_vertex_to_facet((w_.size()*2)-2);
            B.add_vertex_to_facet((w_.size()*2)-1);
            B.add_vertex_to_facet(1);//note order
            B.add_vertex_to_facet(0);
            B.end_facet();
            cout << "added last side facet "  << endl;

            //define top and bottom faces //Todo: might need to triangulate this part already before function receives
//            for(int j = 0;j < 2; j++){
                //bottom => j = 0 ; top => j = 1
                B.begin_facet();
                cout << "facet " << 0 << " : ";
                for (int i = 0; i < w_.size(); i++) {
                    cout << " " << (i*2);
                    B.add_vertex_to_facet((i*2));
                }
                cout << endl;
                B.end_facet();
                B.begin_facet();
                cout << "facet " << 1 << " :";
                for (int i = w_.size()-1; i >= 0 ; i--) {
                    cout << " " << (i*2)+1;
                    B.add_vertex_to_facet((i*2)+1);
                }
                cout << endl;
                B.end_facet();

        }
        B.end_surface();
    }
};

int     main(int argc, char *argv[]) {

    static Point_3 xyz_cube[4] = {
            Point_3(0,0,0),
            Point_3(0,10,0),
            Point_3(10,10,0),
            Point_3(10,0,0)
    };

#define CGAL_MESH_3_VERBOSE 1

    Polyhedron P;
    Extrusion<HalfedgeDS> prism(xyz_cube,4);
    prism.z_thickness = 2;
    P.delegate( prism);
    if(P.is_closed()){
        cout << "Polyhedron is closed " << endl;
    }
    cout << "facets : " <<  P.size_of_facets() << endl;

    cout << "vertices befor triangulation : " << endl;
    Vertex_iterator begin = P.vertices_begin();
    for ( ; begin != P.vertices_end(); ++begin)
        std::cout << "(" << begin->point() << ") ";
    std::cout << std::endl;
    cout << "vertices after triangulation : " << endl;

//    P.make_tetrahedron();

//    Halfedge_handle h = P.make_triangle();
    CGAL::triangulate_polyhedron<Polyhedron>(P);
    cout << "facets : " << P.size_of_facets() << endl;
    Mesh_domain domain(P);

    cout << "facets after triangulation : " << P.size_of_facets() << endl;

    begin = P.vertices_begin();
    for ( ; begin != P.vertices_end(); ++begin)
        std::cout << "(" << begin->point() << ") ";
    std::cout << std::endl;
//    domain.detect_features(0.);
    /**
     * Mesh criteria:
     *
     * facet_angle = 25, facet_size = 0.05, facet_distance = 0.005
     * cell_radius_edge_ratio = 3, cell_size = 0.05
     * */
//    Edge_criteria e_crit(0.025);
    Facet_criteria f_crit(25, 0.5,0.05);
    Cell_criteria c_crit(3,0.1);//,0.05);
    Mesh_criteria criteria(f_crit,
                           c_crit);

    cout << " creating mesh file" << endl;

    domain.detect_features(90);
//    domain.


    cout << " staring make_mesh" << endl;
    
    // Mesh generation
    C3t3 c3t3 = CGAL::make_mesh_3<C3t3>(domain, criteria);

    cout << "facets after meshing : " << c3t3.number_of_facets() << endl;
    cout << "facets in complex after meshing : " << c3t3.number_of_facets_in_complex() << endl;
    cout << "cells after meshing : " << c3t3.number_of_cells() << endl;

//    cout << "writing mesh to file" << endl;
//
//    std::ofstream medit_file("out-with-protection.mesh");
//    cout << "reached program end" << endl;
//    c3t3.output_to_maya(medit_file,true);
//    medit_file.close();


//    // Set tetrahedron size (keep cell_radius_edge_ratio), ignore facets
//    Mesh_criteria new_criteria(cell_radius_edge_ratio=3, cell_size=0.03);
//    // Mesh refinement
//    CGAL::refine_mesh_3(c3t3, domain, new_criteria);
//


//    typedef typename C3t3::Triangulation Triangulation;
//    typedef typename Triangulation::Vertex_handle Vertex_handle;
//
//    const Triangulation& tr = c3t3.triangulation();
//
//    for(typename Tr::Finite_vertices_iterator
//                vit = tr.finite_vertices_begin(),
//                end = tr.finite_vertices_end();
//        vit != end;
//        ++vit)
//    {
//        typedef typename Triangulation::Point Point;
//        const Point& p = vit->point();
//        vtk_points->InsertNextPoint(CGAL::to_double(p.x()),
//                                    CGAL::to_double(p.y()),
//                                    CGAL::to_double(p.z()));
//        V[vit] = inum++;
//    }
//
//    for(typename C3t3::Facets_in_complex_iterator
//                fit = c3t3.facets_in_complex_begin(),
//                end = c3t3.facets_in_complex_end();
//        fit != end; ++fit)
//    {
//        vtkIdType cell[3];
//        int j=0;
//        for (int i = 0; i < 4; ++i)
//            if (i != fit->second)
//                cell[j++] =  V[(*fit).first->vertex(i)];
//        CGAL_assertion(j==3);
//        vtk_facets->InsertNextCell(3, cell);
//    }


//    C3t3_Facet_Iterator facet_begin = c3t3.facets_in_complex_begin();
//    for ( ; facet_begin != c3t3.facets_in_complex_end(); ++facet_begin){
////        *facet_begin.first->;
//        C3t3_Vertex_Iterator vertex_begin = *facet_begin.first->ve;
//
//        for ( ; vertex_begin != c3t3.vertices_in_complex_end(); ++vertex_begin){
//            std::cout << "(" << *vertex_begin << ") ";
//        }
//        std::cout << std::endl;
//    }
//    std::cout << std::endl;






//
//
//    for( C3t3_Facet_Iterator itrFacet = c3t3.facets_in_complex_begin(),
//                 end = c3t3.facets_in_complex_end(); itrFacet != end; ++itrFacet) { // Get the subdomain index for the cell and the opposite cell typename C3T3::Subdomain_index cell_sd = c3t3.subdomain_index( itrFacet->;first );
//
//// Get the vertices of the facet
//
//        for( int j=0, i = 0; i < 4; ++i) {
//            if( i != itrFacet->second) {
//                std::cout << "(" << *itrFacet).first->;vertex(i); << ") ";
//            }
//        }
//

//    C3t3_Vertex_Iterator begin = c3t3.vertices_in_complex_begin();
//    for ( ; begin != c3t3.vertices_in_complex_end(); ++begin)
//        std::cout << "(" << begin.base(). << ") ";
//    std::cout << std::endl;
    return 0;

}


//    Point_2 p(1,1), q(10,10);
//    std::cout << "p = " << p << std::endl;
//    std::cout << "q = " << q.x() << " " << q.y() << std::endl;
//    std::cout << "&q = " << &q.x() << " " << &q.y() << std::endl;
//
//    std::cout << "sqdist(p,q) = "
//    << CGAL::squared_distance(p,q) << std::endl;
//
//    Segment_2 s(p,q);
//    Point_2 m(5, 9);
//
//    std::cout << "m = " << m << std::endl;
//    std::cout << "sqdist(Segment_2(p,q), m) = "
//    << CGAL::squared_distance(s,m) << std::endl;
//    std::cout << "p, q, and m ";
//    switch (CGAL::orientation(p,q,m)){
//        case CGAL::COLLINEAR:
//            std::cout << "are collinear\n";
//            break;
//        case CGAL::LEFT_TURN:
//            std::cout << "make a left turn\n";
//            break;
//        case CGAL::RIGHT_TURN:
//            std::cout << "make a right turn\n";
//            break;
//    }
//    std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;
