#include <iostream>

//#include <CGAL/Simple_cartesian.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/triangulate_polyhedron.h>

//for extrusion
typedef CGAL::Simple_cartesian<double> Kernel;
typedef CGAL::Exact_predicates_inexact_constructions_kernel K;

typedef CGAL::Polygon_2<K> Polygon_2;
typedef Polygon_2::Vertex_iterator VertexIterator;
typedef Polygon_2::Edge_const_iterator EdgeIterator;

typedef K::Point_3 Point_3;
typedef CGAL::Polyhedron_3<K> Polyhedron;
typedef Polyhedron Polygon_3;
typedef Polyhedron::Vertex_iterator Vertex_iterator;
typedef Polyhedron::Halfedge_handle Halfedge_handle;
typedef Polyhedron::HalfedgeDS HalfedgeDS;

//for box intersection tests
#include <CGAL/box_intersection_d.h>
#include <CGAL/Bbox_2.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Boolean_set_operations_2.h>
typedef K::Point_2                                   Point_2;
typedef CGAL::Polygon_2<K>                           Polygon_2;
typedef CGAL::Box_intersection_d::Box_d<double,2> Box;
typedef CGAL::Bbox_2                              Bbox;


#include "print_utils.h"

using namespace std;

                                                     // 9 boxes of a grid
Box boxes[9] = { Bbox( 0,0,1,1), Bbox( 1,0,2,1), Bbox( 2,0,3,1), // low
                 Bbox( 0,1,1,2), Bbox( 1,1,2,2), Bbox( 2,1,3,2), // middle
                 Bbox( 0,2,1,3), Bbox( 1,2,2,3), Bbox( 2,2,3,3)};// upper
// 2 selected boxes as query; center and upper right
Box query[2] = { Bbox( 1,1,2,2), Bbox( 2,2,3,3)};


void callback( const Box& a, const Box& b ) {
    std::cout << "box " << a.id() << " intersects box " << b.id() << std::endl;
}

int     main(int argc, char *argv[]) {


    Polygon_2 P;

    P.push_back (Point_2 (-1,1));
    P.push_back (Point_2 (0,-1));
    P.push_back (Point_2 (1,1));
    std::cout << "P = "; print_polygon (P);
    CGAL::Bbox_2 B1 = P.bbox();
    Polygon_2 Q;
    Q.push_back(Point_2 (-1,1));
    Q.push_back(Point_2 (1,1));
    Q.push_back(Point_2 (0,2));
    CGAL::Bbox_2 B2 = Q.bbox();
    std::cout << "Q = "; print_polygon (Q);
    Polygon_2 W;
    W.push_back(Point_2 (1.1,1));
    W.push_back(Point_2 (2,1));
    W.push_back(Point_2 (2,2));
    CGAL::Bbox_2 B3 = W.bbox();
    std::cout << "W = "; print_polygon (W);
//    Box box1[1] = { Bbox(B1.xmin(),B1.ymin(),B1.xmax(),B1.ymax())};
    Box box1[1] = {B1};
//    Box box2[1] = { Bbox(B2.xmin(),B2.ymin(),B2.xmax(),B2.ymax())};
    std::vector<Box> box2;
    box2.push_back(B2);
    box2.push_back(B3);
//    if ((CGAL::do_intersect(box1, box2)))
    CGAL::box_intersection_d( box1, box1+1, box2.begin(), box2.end(), callback);
//    CGAL::box_intersection_d( B1.begin(), B1.end(), B2.begin(), B2.end(), callback);
//    CGAL::box_intersection_d( B1.begin(), B1.end(), B2.begin(), B2.end(), callback);
//    if(CGAL::box_intersection_d(box1,box1+1,box2,box2B2))
//        std::cout << "The two polygons intersect in their interior." << std::endl;
//    else
//        std::cout << "The two polygons do not intersect." << std::endl;
//
////    CGAL::box_intersection_d( boxes, boxes+9, query, query+2, callback);
    return 0;

}

